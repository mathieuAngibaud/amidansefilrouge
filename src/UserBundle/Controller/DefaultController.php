<?php

namespace UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AmiDanseBundle\Entity\Dancer;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function registerAction(Request $request)
    {
        $user = new Dancer();

        $form = $this->createForm('AmiDanseBundle\Form\DancerType', $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $encoder = $this->container->get('security.password_encoder');
            $encoded = $encoder->encodePassword($user, $user->getPassword());

            $mailContent = $this->renderView(
                'UserBundle:Default:mail.html.twig',
                array('firstName' => $user->getFirstname(),
                    'name' => $user->getName()

                ));

            $mailContentAdmin = $this->renderView(
                'UserBundle:Default:mailAdmin.html.twig',
                array('firstName' => $user->getFirstname(),
                    'name' => $user->getName()

                ));

            $user->setPassword($encoded);
            $user->setRoles(['ROLE_USER']);
            $user->setUsername($user->getEmail());
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();


            $message = \Swift_Message::newInstance()
                ->setSubject('Confirmation d\'inscription')
                ->setFrom('contact.amidanse@gmail.com')
                ->setTo( $user->getEmail() )
                ->setBody($mailContent,
                        'text/html'
                    );

            $messageAdmin = \Swift_Message::newInstance()
                ->setSubject('Nouvelle inscription')
                ->setFrom($user->getEmail())
                ->setTo('contact.amidanse@gmail.com')
                ->setBody($mailContentAdmin,
                    'text/html'
                );

            $this->get('mailer')->send($message);
            $this->get('mailer')->send($messageAdmin);

            return $this->render('AmiDanseBundle:Home:index.html.twig');
        }

        return $this->render('UserBundle:Default:register.html.twig', [
            'form' => $form->createView()
        ]);

    }
}

