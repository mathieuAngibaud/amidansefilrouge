<?php

namespace AmiDanseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class LessonType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date', 'Symfony\Component\Form\Extension\Core\Type\DateType', array(
                'widget' => 'choice',
                'label' => 'Date'
            ))
            ->add('startTime', 'Symfony\Component\Form\Extension\Core\Type\TimeType', array(
                'input'  => 'datetime',
                'widget' => 'choice',
                'label' => 'Heure de début'
            ))
            ->add('type', EntityType::class, array(
                // query choices from this entity
                'class' => 'AmiDanseBundle:Type',
                'choice_label' => 'name',
                'label' => 'Type de danse'
                ))

            ->add('level',TextType::class, array(
                'label' => 'Niveau'
            ))
            ->add('duration',TextType::class, array(
                'label' => 'Durée'
            ))
            ->add('room',TextType::class, array(
                'label' => 'Salle'
            ))
            ->add('description')
            ->add('send', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array(
                'label' => 'Envoyer'
            ));
    }


    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AmidanseBundle\Entity\Lesson'
        ));
    }

    /**
     * {@inheritdoc}
     */
    /*    public function getBlockPrefix()
        {
            return 'registerbundle_register';
        }*/


}
