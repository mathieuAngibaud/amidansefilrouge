<?php

namespace AmiDanseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class OpinionType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder,  array $options)
    {
        $builder 
            ->add('message', 'textarea')
            ->add('send', 'submit', [
                'label' => 'Envoyer',
                'attr'  => [
                    'class' => 'btn btn-primary',
                ],
            ]);
    }


 
}
