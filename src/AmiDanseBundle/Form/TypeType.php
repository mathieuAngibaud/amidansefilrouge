<?php

namespace AmiDanseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class TypeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('name', 'Symfony\Component\Form\Extension\Core\Type\TextType', array(
                'label' => 'Nom du type'
            ))
            ->add('description','Symfony\Component\Form\Extension\Core\Type\TextType', array(
                'label' => 'Description'
            ))
            ->add('style', EntityType::class, array(
                // query choices from this entity
                'class' => 'AmiDanseBundle:Style',

                // use the User.username property as the visible option string
                'choice_label' => 'name'))

//            ->add('send', SubmitType::class);  Symfony 3.0

            ->add('send', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array(
                'label' => 'Envoyer'
            ));

    }


    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AmidanseBundle\Entity\Type'
        ));
    }

    /**
     * {@inheritdoc}
     */
    /*    public function getBlockPrefix()
        {
            return 'registerbundle_register';
        }*/


}
