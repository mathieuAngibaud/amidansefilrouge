<?php

namespace AmiDanseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;


class ProfessorType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder


            ->add('firstName','Symfony\Component\Form\Extension\Core\Type\TextType', array(
                'label' => 'Prénom'
            ))
            ->add('name','Symfony\Component\Form\Extension\Core\Type\TextType', array(
                'label' => 'Nom'
            ))
            ->add('email','Symfony\Component\Form\Extension\Core\Type\EmailType', array(
                'label' => 'Email'
            ))
            ->add('password', 'Symfony\Component\Form\Extension\Core\Type\PasswordType', array(
                'label' => 'Password'
            ))
            ->add('status', 'Symfony\Component\Form\Extension\Core\Type\ChoiceType', array(
                'choices' => array('Interne' => true, 'Externe' => false),
                'choices_as_values' => true,
                'label' => 'Statut'
            ))
            ->add('active', 'Symfony\Component\Form\Extension\Core\Type\ChoiceType', array(
                'choices' => array('Actif' => true, 'Inactif' => false),
                'choices_as_values' => true,
                'label' => 'Actif / Inactif'
            ))
             ->add('sexe', 'Symfony\Component\Form\Extension\Core\Type\ChoiceType', array(
                'choices' => array('Homme' => true, 'Femme' => false),
                'choices_as_values' => true
            ))
            ->add('roles', 'choice', array(
                'label' => 'Rôle:',
                'mapped' => true,
                'expanded' => true,
                'multiple' => true,
                'choices' => array(
                    'ROLE_USER' => 'ROLE_USER',
                    'ROLE_ADMIN' => 'ROLE_ADMIN',
            )))
            ->add('send', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array(
                'label' => 'Envoyer'
            ));
    }


    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AmidanseBundle\Entity\Professor'
        ));
    }

    /**
     * {@inheritdoc}
     */
    /*    public function getBlockPrefix()
        {
            return 'registerbundle_register';
        }*/


}
