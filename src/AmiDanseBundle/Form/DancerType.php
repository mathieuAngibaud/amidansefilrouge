<?php

namespace AmiDanseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;


class DancerType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name','Symfony\Component\Form\Extension\Core\Type\TextType', array(
                'label' => 'Nom'
            ))
            ->add('firstName','Symfony\Component\Form\Extension\Core\Type\TextType', array(
                'label' => 'Prénom'
            ))
            ->add('email','Symfony\Component\Form\Extension\Core\Type\TextType', array(
                'label' => 'Email'
            ))
            ->add('sexe', 'Symfony\Component\Form\Extension\Core\Type\ChoiceType', array(
                'choices' => array('Homme' => true, 'Femme' => false),
                'choices_as_values' => true
            ))

            ->add('password', 'Symfony\Component\Form\Extension\Core\Type\PasswordType')
            ->add('send', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array(
                'label' => 'Envoyer'
            ));
    }


    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AmidanseBundle\Entity\Dancer'
        ));
    }

    /**
     * {@inheritdoc}
     */
    /*    public function getBlockPrefix()
        {
            return 'registerbundle_register';
        }*/


}
