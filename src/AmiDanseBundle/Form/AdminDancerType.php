<?php

namespace AmiDanseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;


class AdminDancerType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class, array(
                'label' => 'Nom'
            ))
            ->add('firstName',TextType::class, array(
                'label' => 'Prénom'
            ))
            ->add('email',TextType::class, array(
                'label' => 'Email'
            ))
            ->add('sexe', ChoiceType::class, array(
                'choices' => array('Homme' => true, 'Femme' => false),
                'choices_as_values' => true
            ))
            ->add('password')
            ->add('roles', 'choice', array(
                'label' => 'Rôle:',
                'mapped' => true,
                'expanded' => true,
                'multiple' => true,
                'choices' => array(
                    'ROLE_USER' => 'ROLE_USER',
                    'ROLE_ADMIN' => 'ROLE_ADMIN',
                )))
            ->add('send', SubmitType::class, array(
                'label' => 'Envoyer'
            ));
    }


    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AmidanseBundle\Entity\Dancer'
        ));
    }

    /**
     * {@inheritdoc}
     */
    /*    public function getBlockPrefix()
        {
            return 'registerbundle_register';
        }*/


}
