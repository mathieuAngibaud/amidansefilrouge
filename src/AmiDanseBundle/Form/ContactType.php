<?php

namespace AmiDanseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;


class ContactType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', 'email', [
                'label' => 'Votre adresse email',
            ])
            ->add('firstName', 'text', [
                'label' => 'Votre prénom',
            ])
            ->add('lastName', 'text', [
                'label' => 'Votre nom',
            ])

            ->add('message', 'textarea', [
                'label' => 'Votre message',
                'attr'  => [
                    'rows' => 8,
                ],
            ])

            ->add('send', 'submit', [
                'label' => 'Envoyer',
                'attr'  => [
                    'class' => 'btn btn-primary',
                ],
            ]);
    }


    /**
     * {@inheritdoc}
     */
//    public function configureOptions(OptionsResolver $resolver)
//    {
//        $resolver->setDefaults(array(
//            'data_class' => 'AmidanseBundle\Entity\Dancer'
//        ));
//    }

    /**
     * {@inheritdoc}
     */
    /*    public function getBlockPrefix()
        {
            return 'registerbundle_register';
        }*/


}
