<?php

namespace AmiDanseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;


class StyleType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('name','Symfony\Component\Form\Extension\Core\Type\TextType', array(
                'label' => 'Nom du Style'
            ))
            ->add('description','Symfony\Component\Form\Extension\Core\Type\TextType', array(
                'label' => 'Description'
            ))

//            ->add('send', SubmitType::class);  Symfony 3.0

            ->add('send', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array(
                'label' => 'Envoyer'
            ));

    }


    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AmidanseBundle\Entity\Style'
        ));
    }

    /**
     * {@inheritdoc}
     */
    /*    public function getBlockPrefix()
        {
            return 'registerbundle_register';
        }*/


}