<?php

namespace AmiDanseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class EventType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        	->add('name')
        	->add('date', 'Symfony\Component\Form\Extension\Core\Type\DateType', array(
        		'input' => 'datetime',
                'widget' => 'choice',
                ))
            ->add('StartTime', 'Symfony\Component\Form\Extension\Core\Type\TimeType', array(
        		'widget' => 'choice',
        		))
        	->add('location')
        	->add('description')
        	->add('send', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array(
                'label' => 'Envoyer'
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AmiDanseBundle\Entity\Event'
        ));
    }

}