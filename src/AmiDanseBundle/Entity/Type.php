<?php

namespace AmiDanseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Type
 */
class Type
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

<<<<<<< HEAD
=======
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $images;
>>>>>>> 046255d80699d922f390d6a955232f445ac6a28a

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Type
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Type
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
<<<<<<< HEAD
=======

      /**
     * Add images
     *
     * @param \AmiDanseBundle\Entity\Image $images
     * @return Type
     */
    public function addImage(\AmiDanseBundle\Entity\Image $images)
    {
        $this->images[] = $images;

        return $this;
    }

    /**
     * Remove images
     *
     * @param \AmiDanseBundle\Entity\Image $images
     */
    public function removeImage(\AmiDanseBundle\Entity\Image $images)
    {
        $this->images->removeElement($images);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @var \AmiDanseBundle\Entity\Style
     */
    private $style;

    /**
     * Set style
     *
     * @param \AmiDanseBundle\Entity\Style $style
     * @return Type
     */
    public function setStyle(\AmiDanseBundle\Entity\Style $style = null)
    {
        $this->style = $style;

        return $this;
    }

    /**
     * Get style
     *
     * @return \AmiDanseBundle\Entity\Style 
     */
    public function getStyle()
    {
        return $this->style;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $lessons;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->lessons = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add lessons
     *
     * @param \AmiDanseBundle\Entity\Lesson $lessons
     * @return Type
     */
    public function addLesson(\AmiDanseBundle\Entity\Lesson $lessons)
    {
        $this->lessons[] = $lessons;

        return $this;
    }

    /**
     * Remove lessons
     *
     * @param \AmiDanseBundle\Entity\Lesson $lessons
     */
    public function removeLesson(\AmiDanseBundle\Entity\Lesson $lessons)
    {
        $this->lessons->removeElement($lessons);
    }

    /**
     * Get lessons
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLessons()
    {
        return $this->lessons;
    }
>>>>>>> 046255d80699d922f390d6a955232f445ac6a28a
}
