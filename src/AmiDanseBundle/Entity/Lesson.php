<?php

namespace AmiDanseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Lesson
 */
class Lesson
{
    /**
<<<<<<< HEAD
     * @var int
=======
     * @var integer
>>>>>>> 046255d80699d922f390d6a955232f445ac6a28a
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var string
     */
    private $level;

    /**
<<<<<<< HEAD
     * @var int
     */
    private $schedule;

    /**
     * @var int
=======
     * @var \DateTime9
     */
    private $startTime;

    /**
     * @var integer
>>>>>>> 046255d80699d922f390d6a955232f445ac6a28a
     */
    private $duration;

    /**
     * @var string
     */
    private $room;

    /**
     * @var string
     */
    private $description;

<<<<<<< HEAD
=======
    /**
     * @var \AmiDanseBundle\Entity\Style
     */
    private $style;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $events;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $professors;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $demonstrations;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $dancers;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->events = new \Doctrine\Common\Collections\ArrayCollection();
        $this->professors = new \Doctrine\Common\Collections\ArrayCollection();
        $this->demonstrations = new \Doctrine\Common\Collections\ArrayCollection();
        $this->dancers = new \Doctrine\Common\Collections\ArrayCollection();
    }
>>>>>>> 046255d80699d922f390d6a955232f445ac6a28a

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Lesson
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set level
     *
     * @param string $level
     * @return Lesson
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return string 
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
<<<<<<< HEAD
     * Set schedule
     *
     * @param integer $schedule
     * @return Lesson
     */
    public function setSchedule($schedule)
    {
        $this->schedule = $schedule;

        return $this;
    }

    /**
     * Get schedule
     *
     * @return integer 
     */
    public function getSchedule()
    {
        return $this->schedule;
    }

    /**
=======
>>>>>>> 046255d80699d922f390d6a955232f445ac6a28a
     * Set duration
     *
     * @param integer $duration
     * @return Lesson
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return integer 
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set room
     *
     * @param string $room
     * @return Lesson
     */
    public function setRoom($room)
    {
        $this->room = $room;

        return $this;
    }

    /**
     * Get room
     *
     * @return string 
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Lesson
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
<<<<<<< HEAD
=======

    /**
     * Set style
     *
     * @param \AmiDanseBundle\Entity\Style $style
     * @return Lesson
     */
    public function setStyle(\AmiDanseBundle\Entity\Style $style = null)
    {
        $this->style = $style;

        return $this;
    }

    /**
     * Get style
     *
     * @return \AmiDanseBundle\Entity\Style 
     */
    public function getStyle()
    {
        return $this->style;
    }

    /**
     * Add events
     *
     * @param \AmiDanseBundle\Entity\Event $events
     * @return Lesson
     */
    public function addEvent(\AmiDanseBundle\Entity\Event $events)
    {
        $this->events[] = $events;

        return $this;
    }

    /**
     * Remove events
     *
     * @param \AmiDanseBundle\Entity\Event $events
     */
    public function removeEvent(\AmiDanseBundle\Entity\Event $events)
    {
        $this->events->removeElement($events);
    }

    /**
     * Get events
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * Add professors
     *
     * @param \AmiDanseBundle\Entity\Professor $professors
     * @return Lesson
     */
    public function addProfessor(\AmiDanseBundle\Entity\Professor $professors)
    {
        $this->professors[] = $professors;

        return $this;
    }

    /**
     * Remove professors
     *
     * @param \AmiDanseBundle\Entity\Professor $professors
     */
    public function removeProfessor(\AmiDanseBundle\Entity\Professor $professors)
    {
        $this->professors->removeElement($professors);
    }

    /**
     * Get professors
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProfessors()
    {
        return $this->professors;
    }

    /**
     * Add demonstrations
     *
     * @param \AmiDanseBundle\Entity\Demonstration $demonstrations
     * @return Lesson
     */
    public function addDemonstration(\AmiDanseBundle\Entity\Demonstration $demonstrations)
    {
        $this->demonstrations[] = $demonstrations;

        return $this;
    }

    /**
     * Remove demonstrations
     *
     * @param \AmiDanseBundle\Entity\Demonstration $demonstrations
     */
    public function removeDemonstration(\AmiDanseBundle\Entity\Demonstration $demonstrations)
    {
        $this->demonstrations->removeElement($demonstrations);
    }

    /**
     * Get demonstrations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDemonstrations()
    {
        return $this->demonstrations;
    }

    /**
     * Add dancers
     *
     * @param \AmiDanseBundle\Entity\Dancer $dancers
     * @return Lesson
     */
    public function addDancer(\AmiDanseBundle\Entity\Dancer $dancers)
    {
        $this->dancers[] = $dancers;

        return $this;
    }

    /**
     * Remove dancers
     *
     * @param \AmiDanseBundle\Entity\Dancer $dancers
     */
    public function removeDancer(\AmiDanseBundle\Entity\Dancer $dancers)
    {
        $this->dancers->removeElement($dancers);
    }

    /**
     * Get dancers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDancers()
    {
        return $this->dancers;
    }


    /**
     * Set startTime
     *
     * @param \DateTime $startTime
     * @return Lesson
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;

        return $this;
    }

    /**
     * Get startTime
     *
     * @return \DateTime 
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * @var \AmiDanseBundle\Entity\Type
     */
    private $type;


    /**
     * Set type
     *
     * @param \AmiDanseBundle\Entity\Type $type
     * @return Lesson
     */
    public function setType(\AmiDanseBundle\Entity\Type $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \AmiDanseBundle\Entity\Type 
     */
    public function getType()
    {
        return $this->type;
    }
    /**
     * @var \AmiDanseBundle\Entity\Professor
     */
    private $professor;


    /**
     * Set professor
     *
     * @param \AmiDanseBundle\Entity\Professor $professor
     * @return Lesson
     */
    public function setProfessor(\AmiDanseBundle\Entity\Professor $professor = null)
    {
        $this->professor = $professor;

        return $this;
    }

    /**
     * Get professor
     *
     * @return \AmiDanseBundle\Entity\Professor 
     */
    public function getProfessor()
    {
        return $this->professor;
    }
>>>>>>> 046255d80699d922f390d6a955232f445ac6a28a
}
