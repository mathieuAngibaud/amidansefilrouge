<?php

namespace AmiDanseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
<<<<<<< HEAD
=======
use Symfony\Component\Security\Core\User\UserInterface;
>>>>>>> 046255d80699d922f390d6a955232f445ac6a28a

/**
 * Professor
 */
<<<<<<< HEAD
class Professor
{
    /**
     * @var int
=======
class Professor implements UserInterface
{
    /**
     * @var integer
>>>>>>> 046255d80699d922f390d6a955232f445ac6a28a
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $firstname;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $password;

    /**
<<<<<<< HEAD
     * @var bool
=======
     * @var boolean
>>>>>>> 046255d80699d922f390d6a955232f445ac6a28a
     */
    private $status;

    /**
<<<<<<< HEAD
     * @var bool
     */
    private $sexe;

=======
     * @var boolean
     */
    private $active;

    /**
     * @var boolean
     */
    private $sexe;

    /**
     * @var array
     */
    private $roles;

    /**
     * @var \AmiDanseBundle\Entity\Image
     */
    private $image;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $proflesson;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $salt;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->proflesson = new \Doctrine\Common\Collections\ArrayCollection();
    }
>>>>>>> 046255d80699d922f390d6a955232f445ac6a28a

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Professor
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return Professor
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Professor
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return Professor
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Professor
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
<<<<<<< HEAD
=======
     * Set active
     *
     * @param boolean $active
     * @return Professor
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()

    {
        return $this->active;
    }

    /**
>>>>>>> 046255d80699d922f390d6a955232f445ac6a28a
     * Set sexe
     *
     * @param boolean $sexe
     * @return Professor
     */
    public function setSexe($sexe)
    {
        $this->sexe = $sexe;

        return $this;
    }

    /**
     * Get sexe
     *
     * @return boolean 
     */
    public function getSexe()
    {
        return $this->sexe;
    }
<<<<<<< HEAD
=======

    /**
     * Set username
     *
     * @param string $username
     * @return Professor
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set roles
     *
     * @param array $roles
     * @return Professor
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * Get roles
     *
     * @return array 
     */
    public function getRoles()
    {
        return $this->roles;
    }


    /**
     * Set image
     *
     * @param \AmiDanseBundle\Entity\Image $image
     * @return Professor
     */
    public function setImage(\AmiDanseBundle\Entity\Image $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \AmiDanseBundle\Entity\Image 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Add proflesson
     *
     * @param \AmiDanseBundle\Entity\Lesson $proflesson
     * @return Professor
     */
    public function addProflesson(\AmiDanseBundle\Entity\Lesson $proflesson)
    {
        $this->proflesson[] = $proflesson;

        return $this;
    }

    /**
     * Remove proflesson
     *
     * @param \AmiDanseBundle\Entity\Lesson $proflesson
     */
    public function removeProflesson(\AmiDanseBundle\Entity\Lesson $proflesson)
    {
        $this->proflesson->removeElement($proflesson);
    }


    public function eraseCredentials(){

    }

    /**
     * Set salt
     *
     * @param string $salt
     *
     * @return Dancer
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Get salt
     *
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->name,
            $this->firstname,
            $this->password,
            $this->sexe,
            $this->email,
            $this->status,
            $this->roles,
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->name,
            $this->firstname,
            $this->password,
            $this->sexe,
            $this->email,
            $this->status,
            $this->roles,
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized);
    }

    /**
     * Get proflesson
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProflesson()
    {
        return $this->proflesson;
    }

>>>>>>> 046255d80699d922f390d6a955232f445ac6a28a
}
