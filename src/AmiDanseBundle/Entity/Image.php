<?php

namespace AmiDanseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Image
 */
class Image
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $file;

    /**
     * @var \DateTime
     */
    private $updateAt;

    /**
     * @var string
     */
    private $alt;

    /**
     * @var \AmiDanseBundle\Entity\Style
     */
    private $style;

    /**
     * @var \AmiDanseBundle\Entity\Type
     */
    private $type;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set file
     *
     * @param string $file
     * @return Image
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return string 
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set updateAt
     *
     * @param \DateTime $updateAt
     * @return Image
     */
    public function setUpdateAt($updateAt)
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    /**
     * Get updateAt
     *
     * @return \DateTime 
     */
    public function getUpdateAt()
    {
        return $this->updateAt;
    }

    /**
     * Set alt
     *
     * @param string $alt
     * @return Image
     */
    public function setAlt($alt)
    {
        $this->alt = $alt;

        return $this;
    }

    /**
     * Get alt
     *
     * @return string 
     */
    public function getAlt()
    {
        return $this->alt;
    }

    /**
     * Set style
     *
     * @param \AmiDanseBundle\Entity\Style $style
     * @return Image
     */
    public function setStyle(\AmiDanseBundle\Entity\Style $style = null)
    {
        $this->style = $style;

        return $this;
    }

    /**
     * Get style
     *
     * @return \AmiDanseBundle\Entity\Style 
     */
    public function getStyle()
    {
        return $this->style;
    }

    /**
     * Set type
     *
     * @param \AmiDanseBundle\Entity\Type $type
     * @return Image
     */
    public function setType(\AmiDanseBundle\Entity\Type $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \AmiDanseBundle\Entity\Type 
     */
    public function getType()
    {
        return $this->type;
    }
    /**
     * @var \AmiDanseBundle\Entity\Post
     */
    private $post;

    /**
     * @var \AmiDanseBundle\Entity\Dancer
     */
    private $dancer;

    /**
     * @var \AmiDanseBundle\Entity\Demonstration
     */
    private $demonstration;

    /**
     * @var \AmiDanseBundle\Entity\Event
     */
    private $event;

    /**
     * @var \AmiDanseBundle\Entity\Professor
     */
    private $professor;

    /**
     * @var \AmiDanseBundle\Entity\Superadmin
     */
    private $superadmin;


    /**
     * Set post
     *
     * @param \AmiDanseBundle\Entity\Post $post
     * @return Image
     */
    public function setPost(\AmiDanseBundle\Entity\Post $post = null)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * Get post
     *
     * @return \AmiDanseBundle\Entity\Post 
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * Set dancer
     *
     * @param \AmiDanseBundle\Entity\Dancer $dancer
     * @return Image
     */
    public function setDancer(\AmiDanseBundle\Entity\Dancer $dancer = null)
    {
        $this->dancer = $dancer;

        return $this;
    }

    /**
     * Get dancer
     *
     * @return \AmiDanseBundle\Entity\Dancer 
     */
    public function getDancer()
    {
        return $this->dancer;
    }

    /**
     * Set demonstration
     *
     * @param \AmiDanseBundle\Entity\Demonstration $demonstration
     * @return Image
     */
    public function setDemonstration(\AmiDanseBundle\Entity\Demonstration $demonstration = null)
    {
        $this->demonstration = $demonstration;

        return $this;
    }

    /**
     * Get demonstration
     *
     * @return \AmiDanseBundle\Entity\Demonstration 
     */
    public function getDemonstration()
    {
        return $this->demonstration;
    }

    /**
     * Set event
     *
     * @param \AmiDanseBundle\Entity\Event $event
     * @return Image
     */
    public function setEvent(\AmiDanseBundle\Entity\Event $event = null)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * Get event
     *
     * @return \AmiDanseBundle\Entity\Event 
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Set professor
     *
     * @param \AmiDanseBundle\Entity\Professor $professor
     * @return Image
     */
    public function setProfessor(\AmiDanseBundle\Entity\Professor $professor = null)
    {
        $this->professor = $professor;

        return $this;
    }

    /**
     * Get professor
     *
     * @return \AmiDanseBundle\Entity\Professor 
     */
    public function getProfessor()
    {
        return $this->professor;
    }

    /**
     * Set superadmin
     *
     * @param \AmiDanseBundle\Entity\Superadmin $superadmin
     * @return Image
     */
    public function setSuperadmin(\AmiDanseBundle\Entity\Superadmin $superadmin = null)
    {
        $this->superadmin = $superadmin;

        return $this;
    }

    /**
     * Get superadmin
     *
     * @return \AmiDanseBundle\Entity\Superadmin 
     */
    public function getSuperadmin()
    {
        return $this->superadmin;
    }
}
