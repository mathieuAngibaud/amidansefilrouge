<?php

namespace AmiDanseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Event
 */
class Event
{
    /**
<<<<<<< HEAD
     * @var int
=======
     * @var integer
>>>>>>> 046255d80699d922f390d6a955232f445ac6a28a
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \DateTime
     */
    private $date;

    /**
<<<<<<< HEAD
     * @var int
     */
    private $hour;
=======
     * @var \DateTime
     */
    private $start_time;
>>>>>>> 046255d80699d922f390d6a955232f445ac6a28a

    /**
     * @var string
     */
    private $location;

    /**
     * @var string
     */
    private $description;

<<<<<<< HEAD
=======
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $demos;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $eventlesson;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->demos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->eventlesson = new \Doctrine\Common\Collections\ArrayCollection();
    }
>>>>>>> 046255d80699d922f390d6a955232f445ac6a28a

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Event
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Event
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
<<<<<<< HEAD
     * Set hour
     *
     * @param integer $hour
     * @return Event
     */
    public function setHour($hour)
    {
        $this->hour = $hour;
=======
     * Set startTime
     *
     * @param \DateTime $startTime
     * @return Lesson
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;
>>>>>>> 046255d80699d922f390d6a955232f445ac6a28a

        return $this;
    }

    /**
<<<<<<< HEAD
     * Get hour
     *
     * @return integer 
     */
    public function getHour()
    {
        return $this->hour;
=======
     * Get startTime
     *
     * @return \DateTime 
     */
    public function getStartTime()
    {
        return $this->startTime;
>>>>>>> 046255d80699d922f390d6a955232f445ac6a28a
    }

    /**
     * Set location
     *
     * @param string $location
     * @return Event
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Event
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
<<<<<<< HEAD
=======

    /**
     * Add demos
     *
     * @param \AmiDanseBundle\Entity\Demonstration $demos
     * @return Event
     */
    public function addDemo(\AmiDanseBundle\Entity\Demonstration $demos)
    {
        $this->demos[] = $demos;

        return $this;
    }

    /**
     * Remove demos
     *
     * @param \AmiDanseBundle\Entity\Demonstration $demos
     */
    public function removeDemo(\AmiDanseBundle\Entity\Demonstration $demos)
    {
        $this->demos->removeElement($demos);
    }

    /**
     * Get demos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDemos()
    {
        return $this->demos;
    }


    /**
     * Add eventlesson
     *
     * @param \AmiDanseBundle\Entity\Lesson $eventlesson
     * @return Event
     */
    public function addEventlesson(\AmiDanseBundle\Entity\Lesson $eventlesson)
    {
        $this->eventlesson[] = $eventlesson;

        return $this;
    }

    /**
     * Remove eventlesson
     *
     * @param \AmiDanseBundle\Entity\Lesson $eventlesson
     */
    public function removeEventlesson(\AmiDanseBundle\Entity\Lesson $eventlesson)
    {
        $this->eventlesson->removeElement($eventlesson);
    }

    /**
     * Get eventlesson
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEventlesson()
    {
        return $this->eventlesson;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $images;


    /**
     * Add images
     *
     * @param \AmiDanseBundle\Entity\Image $images
     * @return Event
     */
    public function addImage(\AmiDanseBundle\Entity\Image $images)
    {
        $this->images[] = $images;


        return $this;
    }

    /**
     * Remove images
     *
     * @param \AmiDanseBundle\Entity\Image $images
     */
    public function removeImage(\AmiDanseBundle\Entity\Image $images)
    {
        $this->images->removeElement($images);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @var \DateTime
     */
    private $startTime;

>>>>>>> 046255d80699d922f390d6a955232f445ac6a28a
}
