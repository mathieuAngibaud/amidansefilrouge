<?php

namespace AmiDanseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
<<<<<<< HEAD
=======
use Symfony\Component\Security\Core\User\UserInterface;
>>>>>>> 046255d80699d922f390d6a955232f445ac6a28a

/**
 * Dancer
 */
<<<<<<< HEAD
class Dancer
=======
class Dancer implements UserInterface
>>>>>>> 046255d80699d922f390d6a955232f445ac6a28a
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $firstname;

    /**
     * @var bool
     */
    private $sexe;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $password;

    /**
     * @var bool
     */
    private $status;

<<<<<<< HEAD
=======
    /**
     * @var array
     */
    private $roles;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $salt;

>>>>>>> 046255d80699d922f390d6a955232f445ac6a28a

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Dancer
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return Dancer
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set sexe
     *
     * @param boolean $sexe
     * @return Dancer
     */
    public function setSexe($sexe)
    {
        $this->sexe = $sexe;

        return $this;
    }

    /**
     * Get sexe
     *
     * @return boolean 
     */
    public function getSexe()
    {
        return $this->sexe;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Dancer
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return Dancer
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Dancer
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }
<<<<<<< HEAD
=======
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $dancerlesson;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dancerlesson = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add dancerlesson
     *
     * @param \AmiDanseBundle\Entity\Lesson $dancerlesson
     * @return Dancer
     */
    public function addDancerlesson(\AmiDanseBundle\Entity\Lesson $dancerlesson)
    {
        $this->dancerlesson[] = $dancerlesson;

        return $this;
    }

    /**
     * Remove dancerlesson
     *
     * @param \AmiDanseBundle\Entity\Lesson $dancerlesson
     */
    public function removeDancerlesson(\AmiDanseBundle\Entity\Lesson $dancerlesson)
    {
        $this->dancerlesson->removeElement($dancerlesson);
    }

    /**
     * Get dancerlesson
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDancerlesson()
    {
        return $this->dancerlesson;
    }



    /**
     * Set roles
     *
     * @param array $roles
     * @return Dancer
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * Get roles
     *
     * @return array 
     */
    public function getRoles()
    {
        return $this->roles;
    }
    /**
     * Set salt
     *
     * @param string $salt
     *
     * @return Dancer
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Get salt
     *
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return Dancer
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get userName
     *
     * @return string
     */
    public function getUserName()
    {
        return $this->username;
    }


    public function eraseCredentials(){

    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->name,
            $this->firstname,
            $this->password,
            $this->sexe,
            $this->email,
            $this->status,
            $this->roles,
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->name,
            $this->firstname,
            $this->password,
            $this->sexe,
            $this->email,
            $this->status,
            $this->roles,
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized);
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $image;


    /**
     * Add image
     *
     * @param \AmiDanseBundle\Entity\Image $image
     * @return Dancer
     */
    public function addImage(\AmiDanseBundle\Entity\Image $image)
    {
        $this->image[] = $image;

        return $this;
    }

    /**
     * Remove image
     *
     * @param \AmiDanseBundle\Entity\Image $image
     */
    public function removeImage(\AmiDanseBundle\Entity\Image $image)
    {
        $this->image->removeElement($image);
    }

    /**
     * Get image
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set image
     *
     * @param \AmiDanseBundle\Entity\Image $image
     * @return Dancer
     */
    public function setImage(\AmiDanseBundle\Entity\Image $image = null)
    {
        $this->image = $image;

        return $this;
    }
>>>>>>> 046255d80699d922f390d6a955232f445ac6a28a
}
