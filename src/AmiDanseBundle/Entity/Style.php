<?php

namespace AmiDanseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Style
 */
class Style
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

<<<<<<< HEAD
=======
    /**
     * @var \AmiDanseBundle\Entity\Image
     */
    private $image;
>>>>>>> 046255d80699d922f390d6a955232f445ac6a28a

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Style
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Style
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
<<<<<<< HEAD
=======

    /**
     * Set image
     *
     * @param \AmiDanseBundle\Entity\Image $image
     * @return Style
     */
    public function setImage(\AmiDanseBundle\Entity\Image $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \AmiDanseBundle\Entity\Image 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $lessons;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->lessons = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add lessons
     *
     * @param \AmiDanseBundle\Entity\Lesson $lessons
     * @return Style
     */
    public function addLesson(\AmiDanseBundle\Entity\Lesson $lessons)
    {
        $this->lessons[] = $lessons;

        return $this;
    }

    /**
     * Remove lessons
     *
     * @param \AmiDanseBundle\Entity\Lesson $lessons
     */
    public function removeLesson(\AmiDanseBundle\Entity\Lesson $lessons)
    {
        $this->lessons->removeElement($lessons);
    }

    /**
     * Get lessons
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLessons()
    {
        return $this->lessons;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $styles;


    /**
     * Add styles
     *
     * @param \AmiDanseBundle\Entity\Type $styles
     * @return Style
     */
    public function addStyle(\AmiDanseBundle\Entity\Type $styles)
    {
        $this->styles[] = $styles;

        return $this;
    }

    /**
     * Remove styles
     *
     * @param \AmiDanseBundle\Entity\Type $styles
     */
    public function removeStyle(\AmiDanseBundle\Entity\Type $styles)
    {
        $this->styles->removeElement($styles);
    }

    /**
     * Get styles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getStyles()
    {
        return $this->styles;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $type;


    /**
     * Add type
     *
     * @param \AmiDanseBundle\Entity\Type $type
     * @return Style
     */
    public function addType(\AmiDanseBundle\Entity\Type $type)
    {
        $this->type[] = $type;

        return $this;
    }

    /**
     * Remove type
     *
     * @param \AmiDanseBundle\Entity\Type $type
     */
    public function removeType(\AmiDanseBundle\Entity\Type $type)
    {
        $this->type->removeElement($type);
    }

    /**
     * Get type
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getType()
    {
        return $this->type;
    }
>>>>>>> 046255d80699d922f390d6a955232f445ac6a28a
}
