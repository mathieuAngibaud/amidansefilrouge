<?php
namespace AmiDanseBundle\Controller;
use AmiDanseBundle\Entity\Dancer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AmiDanseBundle\Form\DancerType;

class DancerController extends Controller
{
    public function profileAction()
    {
        return $this->render('AmiDanseBundle:Dancer:index.html.twig');
    }

    public function editAction(Request $request, Dancer $dancer)
    {
        
        $editForm = $this->createForm('AmiDanseBundle\Form\DancerType', $dancer);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('amidanse_dancer_profile');
        }

        return $this->render('AmiDanseBundle:Dancer:edit.html.twig', array(
            'dancer' => $dancer,
            'edit_form' => $editForm->createView(),
            
        ));
    }


      public function opinionAction(Request $request)
    {

        $form = $this->createForm('AmiDanseBundle\Form\OpinionType');
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $user = $this->get('security.token_storage')->getToken()->getUser();
            $message = $this->renderView(
                'AmiDanseBundle:Dancer:mail.html.twig',
                array('message' => $form["message"]->getData()
                ));

            $mail = \Swift_Message::newInstance()
                ->setSubject('Vous avez reçu l\'avis d\'un membre')
                ->setTo('contact.amidanse@gmail.com')
                ->setFrom($user->getEmail())
                ->setBody($message,
                'text/html'
                );

                $this->get('mailer')->send($mail);

                return $this->render('AmiDanseBundle:Dancer:index.html.twig');
    }

     return $this->render('AmiDanseBundle:Dancer:opinion.html.twig', array(
            'form' => $form->createView(),
            
    ));

    }


}