<?php
namespace AmiDanseBundle\Controller;

use AmiDanseBundle\Entity\Type;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;



class StyleController extends Controller
{       

    // list all the style
     public function indexAction()
    {
        $styles = $this
            ->getDoctrine()
            ->getRepository('AmiDanseBundle:Style')
            ->findAll();

        return $this->render('AmiDanseBundle:Style:index.html.twig', [
            'styles' => $styles,
        ]);
    }

    // retourne tous les types en fonction du style

       public function styleAction($styleName)
    {

        $style = $this
            ->getDoctrine()
            ->getRepository('AmiDanseBundle:Style')
            ->findOneBy(['name' => $styleName]);
        if (!$style) {
            throw $this->createNotFoundException('Style of danse not found.');
        }  
        
        $types = $this
            ->getDoctrine()
            ->getRepository('AmiDanseBundle:Type')
            ->findByStyle($style);

       
        return $this->render('AmiDanseBundle:Style:style.html.twig', [
            'style' => $style,
            'types' => $types,
            // 'images'=> $images,
        ]);
       
    }



    // return one type with description etc 
     public function typeAction($styleName, $typeName)
    {
       
        $style = $this
            ->getDoctrine()
            ->getRepository('AmiDanseBundle:Style')
            ->findOneBy(['name' => $styleName]);  

        $type = $this
            ->getDoctrine()
            ->getRepository('AmiDanseBundle:Type')
            ->findOneBy([
              'style'=>$style,
               'name'=>$typeName,
               ]);

            
      if (!$type) {
            throw $this->createNotFoundException('Type not found.');
            
          }


        $lessons = $this
           ->getDoctrine()
           ->getRepository('AmiDanseBundle:Lesson')
           ->findBy([
               'type'=>$type,

               ]);

        $images = $this
           ->getDoctrine()
           ->getRepository('AmiDanseBundle:Image')
           ->findBy([
               'type'=>$type,
                
               ]);
        
                   
        return $this->render('AmiDanseBundle:Style:type.html.twig', [
            'type' => $type,
            'lessons'=>$lessons,
            'images'=>$images,
            
        ]);


      

    }

}