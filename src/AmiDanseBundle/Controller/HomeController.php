<?php

namespace AmiDanseBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HomeController extends Controller
{
    public function indexAction()
    {
        return $this->render('AmiDanseBundle:Home:index.html.twig');
    }
}
