<?php

namespace AmiDanseBundle\Controller;

use AmiDanseBundle\Entity\Demonstration;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DemonstrationController extends Controller
{
    public function indexAction()
    {
    	$demonstrations = $this
            ->getDoctrine()
            ->getRepository('AmiDanseBundle:Demonstration')
            ->findAll();

        return $this->render('AmiDanseBundle:Demonstration:index.html.twig', [
            'names' => $demonstrations,
        ]);
    }
}
