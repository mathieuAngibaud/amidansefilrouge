<?php

namespace AmiDanseBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class FooterController extends Controller
{
    public function mapAction()
    {
        return $this->render('AmiDanseBundle:Mapsite:index.html.twig');
    }

    public function mentionsAction()
    {
        return $this->render('AmiDanseBundle:Mentions:index.html.twig');
    }
}
