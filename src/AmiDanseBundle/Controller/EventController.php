<?php

namespace AmiDanseBundle\Controller;

use AmiDanseBundle\Entity\Event;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class EventController extends Controller
{
    
 public function indexAction()
    {
        $events = $this
            ->getDoctrine()
            ->getRepository('AmiDanseBundle:Event')
            ->findAll();

        return $this->render('AmiDanseBundle:Event:index.html.twig', [
            'names' => $events,
        ]);
    }
    	
}
