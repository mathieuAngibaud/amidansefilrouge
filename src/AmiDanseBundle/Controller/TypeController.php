<?php
namespace AmiDanseBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Type controller.
 *
 */
class TypeController extends Controller
{
    /**
     * Lists all types entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $types = $em->getRepository('AmiDanseBundle:Type')->findAll();

        return $this->render('AmiDanseBundle:type:index.html.twig', array(
            'types' => $types,
        ));
    }
}