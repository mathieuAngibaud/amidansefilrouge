<?php

namespace AmiDanseBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AmiDanseBundle\Entity\Demonstration;
use Symfony\Component\HttpFoundation\Request;
use AmiDanseBundle\Form\StyleType;

class AdminDemonstrationController extends Controller
{

	public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $demonstrations = $em->getRepository('AmiDanseBundle:Demonstration')->findAll();

        return $this->render('AmiDanseBundle:Admin/Demonstration:index.html.twig', array(
            'demonstrations' => $demonstrations,
        ));
    }

    public function newAction(Request $request)
    {
        $demonstration = new Demonstration();

        $form = $this->createForm('AmiDanseBundle\Form\DemonstrationType', $demonstration);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($demonstration);
            $em->flush();

            return $this->redirectToRoute('amidanse_admin_demonstration_index');
        }

        return $this->render('AmiDanseBundle:Admin/Demonstration:new.html.twig', [
            'form' => $form->createView()
        ]);

    }

    public function showAction(Demonstration $demonstration)
    {
        $deleteForm = $this->createDeleteForm($demonstration);

        return $this->render('AmiDanseBundle:Admin/Demonstration:show.html.twig', array(
            'demonstration' => $demonstration,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    public function editAction(Request $request, Demonstration $demonstration)
    {
        $deleteForm = $this->createDeleteForm($demonstration);
        $editForm = $this->createForm('AmiDanseBundle\Form\DemonstrationType', $demonstration);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('amidanse_admin_demonstration_index', array('id' => $demonstration->getId()));
        }

        return $this->render('AmiDanseBundle:Admin/Demonstration:edit.html.twig', array(
            'demonstration' => $demonstration,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    public function deleteAction(Request $request, Demonstration $demonstration)
    {
        $form = $this->createDeleteForm($demonstration);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($demonstration);
            $em->flush();
        }

        return $this->redirectToRoute('amidanse_admin_demonstration_index');
    }

    private function createDeleteForm(Demonstration $demonstration)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('amidanse_admin_demonstration_delete', array('id' => $demonstration->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }

}