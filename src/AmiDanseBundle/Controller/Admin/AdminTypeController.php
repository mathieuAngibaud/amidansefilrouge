<?php
/**
 * Created by PhpStorm.
 * User: matflam
 * Date: 5/3/17
 * Time: 8:18 PM
 */

namespace AmiDanseBundle\Controller\Admin;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AmiDanseBundle\Entity\Type;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;



class AdminTypeController extends Controller
{
    /**
     * Lists all category entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $types = $em->getRepository('AmiDanseBundle:Type')->findAll();
        $styles = $em->getRepository('AmiDanseBundle:Style')->findAll();

        return $this->render('AmiDanseBundle:Admin/Type:index.html.twig', array(
            'types' => $types,
            'styles' => $styles
        ));
    }

    public function newAction(Request $request)
    {
        $type = new Type();

        $form = $this->createForm('AmiDanseBundle\Form\TypeType', $type);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($type);
            $em->flush();

            return $this->redirectToRoute('amidanse_admin_type_index');
        }

        return $this->render('AmiDanseBundle:Admin/Type:new.html.twig', [
            'form' => $form->createView()
        ]);

    }

    /**
     * Finds and displays a style entity.
     *
     */
    public function showAction(Type $type)
    {
        $deleteForm = $this->createDeleteForm($type);

        return $this->render('AmiDanseBundle:Admin/Type:show.html.twig', array(
            'type' => $type,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing category entity.
     *
     */
    public function editAction(Request $request, Type $type)
    {
        $deleteForm = $this->createDeleteForm($type);
        $editForm = $this->createForm('AmiDanseBundle\Form\StyleType', $type);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('amidanse_admin_type_index', array('id' => $type->getId()));
        }

        return $this->render('AmiDanseBundle:Admin/Type:edit.html.twig', array(
            'type' => $type,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a style entity.
     *
     */
    public function deleteAction(Request $request, Type $type)
    {
        // On vérifie que l'utilisateur dispose bien du rôle ROLE_AUTEUR
        if (!$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            // Sinon on déclenche une exception « Accès interdit »
            throw new AccessDeniedException('Accès limité aux Super-administrateurs!!!');
        }

        $form = $this->createDeleteForm($type);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($type);
            $em->flush();
        }

        return $this->redirectToRoute('amidanse_admin_style_index');
    }

    private function createDeleteForm(Type $type)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('amidanse_admin_type_delete', array('id' => $type->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }
}