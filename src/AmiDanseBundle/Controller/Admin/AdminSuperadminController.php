<?php

namespace AmiDanseBundle\Controller\Admin;

use AmiDanseBundle\Entity\Superadmin;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use AmiDanseBundle\Form\SuperadminType;

class AdminSuperadminController extends Controller
{
    /**
     * Lists all superadmin entities.
     *
     */
    public function indexAction()
    {
        // On vérifie que l'utilisateur dispose bien du rôle ROLE_AUTEUR
        if (!$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            // Sinon on déclenche une exception « Accès interdit »
            throw new AccessDeniedException('Accès limité aux Super-administrateurs!!!');
        }

        $em = $this->getDoctrine()->getManager();

        $superadmins = $em->getRepository('AmiDanseBundle:Superadmin')->findAll();

        return $this->render('AmiDanseBundle:Admin/Superadmin:index.html.twig', array(
            'superadmins' => $superadmins,
        ));
    }

    public function newAction(Request $request)
    {
        // On vérifie que l'utilisateur dispose bien du rôle ROLE_AUTEUR
        if (!$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            // Sinon on déclenche une exception « Accès interdit »
            throw new AccessDeniedException('Accès limité aux Super-administrateurs!!!');
        }

        $superadmin = new Superadmin();

        $form = $this->createForm('AmiDanseBundle\Form\SuperadminType', $superadmin);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $encoder = $this->container->get('security.password_encoder');
            $encoded = $encoder->encodePassword($superadmin, $superadmin->getPassword());

            $superadmin->setPassword($encoded);

            $superadmin->setUsername($superadmin->getEmail());
            $em = $this->getDoctrine()->getManager();
            $em->persist($superadmin);
            $em->flush();

            return $this->redirectToRoute('amidanse_admin_superadmin_index');
        }

        return $this->render('AmiDanseBundle:Admin/Superadmin:new.html.twig', [
            'form' => $form->createView()
        ]);

    }

    /**
     * Finds and displays a superadmin entity.
     *
     */
    public function showAction(Superadmin $superadmin)
    {
        // On vérifie que l'utilisateur dispose bien du rôle ROLE_AUTEUR
        if (!$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            // Sinon on déclenche une exception « Accès interdit »
            throw new AccessDeniedException('Accès limité aux Super-administrateurs!!!');
        }

        $deleteForm = $this->createDeleteForm($superadmin);

        return $this->render('AmiDanseBundle:Admin/Superadmin:show.html.twig', array(
            'superadmin' => $superadmin,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing superadmin entity.
     *
     */
    public function editAction(Request $request, Superadmin $superadmin)
    {
        // On vérifie que l'utilisateur dispose bien du rôle ROLE_AUTEUR
        if (!$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            // Sinon on déclenche une exception « Accès interdit »
            throw new AccessDeniedException('Accès limité aux Super-administrateurs!!!');
        }

        $deleteForm = $this->createDeleteForm($superadmin);
        $editForm = $this->createForm('AmiDanseBundle\Form\SuperadminType', $superadmin);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $superadmin->setUsername($superadmin->getEmail());
            $encoder = $this->container->get('security.password_encoder');
            $encoded = $encoder->encodePassword($superadmin, $superadmin->getPassword());

            $superadmin->setPassword($encoded);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('amidanse_admin_superadmin_index', array('id' => $superadmin->getId()));
        }

        return $this->render('AmiDanseBundle:Admin/Superadmin:edit.html.twig', array(
            'superadmin' => $superadmin,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a superadmin entity.
     *
     */
    public function deleteAction(Request $request, Superadmin $superadmin)
    {
        // On vérifie que l'utilisateur dispose bien du rôle ROLE_AUTEUR
        if (!$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            // Sinon on déclenche une exception « Accès interdit »
            throw new AccessDeniedException('Accès limité aux Super-administrateurs!!!');
        }

        $form = $this->createDeleteForm($superadmin);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($superadmin);
            $em->flush();
        }

        return $this->redirectToRoute('amidanse_admin_superadmin_index');
    }

    private function createDeleteForm(Superadmin $superadmin)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('amidanse_admin_superadmin_delete', array('id' => $superadmin->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }
}

