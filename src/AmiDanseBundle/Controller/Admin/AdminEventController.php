<?php

namespace AmiDanseBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AmiDanseBundle\Entity\Event;
use Symfony\Component\HttpFoundation\Request;
use AmiDanseBundle\Form\StyleType;

class AdminEventController extends Controller
{

	public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $events = $em->getRepository('AmiDanseBundle:Event')->findAll();

        return $this->render('AmiDanseBundle:Admin/Event:index.html.twig', array(
            'events' => $events,
        ));
    }

    public function newAction(Request $request)
    {
        $event = new Event();

        $form = $this->createForm('AmiDanseBundle\Form\EventType', $event);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($event);
            $em->flush();

            return $this->redirectToRoute('amidanse_admin_event_index');
        }

        return $this->render('AmiDanseBundle:Admin/Event:new.html.twig', [
            'form' => $form->createView()
        ]);

    }

    public function showAction(Event $event)
    {
        $deleteForm = $this->createDeleteForm($event);

        return $this->render('AmiDanseBundle:Admin/Event:show.html.twig', array(
            'event' => $event,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    public function editAction(Request $request, Event $event)
    {
        $deleteForm = $this->createDeleteForm($event);
        $editForm = $this->createForm('AmiDanseBundle\Form\EventType', $event);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('amidanse_admin_event_index', array('id' => $event->getId()));
        }

        return $this->render('AmiDanseBundle:Admin/Event:edit.html.twig', array(
            'event' => $event,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    public function deleteAction(Request $request, Event $event)
    {
        $form = $this->createDeleteForm($event);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($event);
            $em->flush();
        }

        return $this->redirectToRoute('amidanse_admin_event_index');
    }

    private function createDeleteForm(Event $event)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('amidanse_admin_event_delete', array('id' => $event->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }

}