<?php

namespace AmiDanseBundle\Controller\Admin;

use AmiDanseBundle\Entity\Professor;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AmiDanseBundle\Controller\Admin\HashController;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use AmiDanseBundle\Form\ProfessorType;

class AdminProfessorController extends Controller
{
    /**
     * Lists all professor entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $professors = $em->getRepository('AmiDanseBundle:Professor')->findAll();

        return $this->render('AmiDanseBundle:Admin/Professor:index.html.twig', array(
            'professors' => $professors,
        ));
    }

    public function newAction(Request $request)
    {
        // On vérifie que l'utilisateur dispose bien du rôle ROLE_AUTEUR
        if (!$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            // Sinon on déclenche une exception « Accès interdit »
            throw new AccessDeniedException('Accès limité aux Super-administrateurs!!!');
        }

        $professor = new Professor();

        $form = $this->createForm('AmiDanseBundle\Form\ProfessorType', $professor);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $encoder = $this->container->get('security.password_encoder');
            $encoded = $encoder->encodePassword($professor, $professor->getPassword());


            $professor->setPassword($encoded);

            $professor->setUsername($professor->getEmail());

            $em = $this->getDoctrine()->getManager();
            $em->persist($professor);
            $em->flush();

            return $this->redirectToRoute('amidanse_admin_professor_index');
        }

        return $this->render('AmiDanseBundle:Admin/Professor:new.html.twig', [
            'form' => $form->createView()
        ]);

    }

    /**
     * Finds and displays a professor entity.
     *
     */
    public function showAction(Professor $professor)
    {
        $deleteForm = $this->createDeleteForm($professor);

        return $this->render('AmiDanseBundle:Admin/Professor:show.html.twig', array(
            'professor' => $professor,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing category entity.
     *
     */
    public function editAction(Request $request, Professor $professor)
    {
        // On vérifie que l'utilisateur dispose bien du rôle ROLE_AUTEUR
        if (!$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            // Sinon on déclenche une exception « Accès interdit »
            throw new AccessDeniedException('Accès limité aux Super-administrateurs!!!');
        }

        $deleteForm = $this->createDeleteForm($professor);
        $editForm = $this->createForm('AmiDanseBundle\Form\ProfessorType', $professor);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $encoder = $this->container->get('security.password_encoder');
            $encoded = $encoder->encodePassword($professor, $professor->getPassword());

            $professor->setPassword($encoded);

            $professor->setUsername($professor->getEmail());
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('amidanse_admin_professor_index', array('id' => $professor->getId()));
        }

        return $this->render('AmiDanseBundle:Admin/Professor:edit.html.twig', array(
            'professor' => $professor,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a style entity.
     *
     */
    public function deleteAction(Request $request, Professor $professor)
    {
        // On vérifie que l'utilisateur dispose bien du rôle ROLE_AUTEUR
        if (!$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            // Sinon on déclenche une exception « Accès interdit »
            throw new AccessDeniedException('Accès limité aux Super-administrateurs!!!');
        }

        $form = $this->createDeleteForm($professor);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($professor);
            $em->flush();
        }

        return $this->redirectToRoute('amidanse_admin_professor_index');
    }

    private function createDeleteForm(Professor $professor)
    {

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('amidanse_admin_professor_delete', array('id' => $professor->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }
}

