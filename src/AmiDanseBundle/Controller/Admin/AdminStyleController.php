<?php

namespace AmiDanseBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AmiDanseBundle\Entity\Style;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use AmiDanseBundle\Form\StyleType;

class AdminStyleController extends Controller
{
    /**
     * Lists all category entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $styles = $em->getRepository('AmiDanseBundle:Style')->findAll();

        return $this->render('AmiDanseBundle:Admin/Style:index.html.twig', array(
            'styles' => $styles,
        ));
    }

    public function newAction(Request $request)
    {
        $style = new Style();

        $form = $this->createForm('AmiDanseBundle\Form\StyleType', $style);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($style);
            $em->flush();

            return $this->redirectToRoute('amidanse_admin_style_index');
        }

        return $this->render('AmiDanseBundle:Admin/Style:new.html.twig', [
            'form' => $form->createView()
        ]);

    }

    /**
     * Finds and displays a style entity.
     *
     */
    public function showAction(Style $style)
    {
        $deleteForm = $this->createDeleteForm($style);

        return $this->render('AmiDanseBundle:Admin/Style:show.html.twig', array(
            'style' => $style,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing category entity.
     *
     */
    public function editAction(Request $request, Style $style)
    {
        $deleteForm = $this->createDeleteForm($style);
        $editForm = $this->createForm('AmiDanseBundle\Form\StyleType', $style);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('amidanse_admin_style_index', array('id' => $style->getId()));
        }

        return $this->render('AmiDanseBundle:Admin/Style:edit.html.twig', array(
            'style' => $style,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a style entity.
     *
     */
    public function deleteAction(Request $request, Style $style)
    {
        // On vérifie que l'utilisateur dispose bien du rôle ROLE_AUTEUR
        if (!$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            // Sinon on déclenche une exception « Accès interdit »
            throw new AccessDeniedException('Accès limité aux Super-administrateurs!!!');
        }

        $form = $this->createDeleteForm($style);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($style);
            $em->flush();
        }

        return $this->redirectToRoute('amidanse_admin_style_index');
    }

    private function createDeleteForm(Style $style)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('amidanse_admin_style_delete', array('id' => $style->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }
}

