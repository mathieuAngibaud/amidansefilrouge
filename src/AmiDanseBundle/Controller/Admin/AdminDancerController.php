<?php

namespace AmiDanseBundle\Controller\Admin;

use AmiDanseBundle\Entity\Dancer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use AmiDanseBundle\Form\AdminDancerType;

class AdminDancerController extends Controller
{
    /**
     * Lists all professor entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $dancers = $em->getRepository('AmiDanseBundle:Dancer')->findAll();

        return $this->render('AmiDanseBundle:Admin/Dancer:index.html.twig', array(
            'dancers' => $dancers,
        ));
    }

    public function newAction(Request $request)
    {
        // On vérifie que l'utilisateur dispose bien du rôle ROLE_AUTEUR
        if (!$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            // Sinon on déclenche une exception « Accès interdit »
            throw new AccessDeniedException('Accès limité aux Super-administrateurs!!!');
        }

        $dancer = new Dancer();

        $form = $this->createForm('AmiDanseBundle\Form\AdminDancerType', $dancer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $encoder = $this->container->get('security.password_encoder');
            $encoded = $encoder->encodePassword($user, $user->getPassword());

            $user->setPassword($encoded);

            $em = $this->getDoctrine()->getManager();
            $em->persist($dancer);
            $em->flush();

            return $this->redirectToRoute('amidanse_admin_dancer_index');
        }

        return $this->render('AmiDanseBundle:Admin/Dancer:new.html.twig', [
            'form' => $form->createView()
        ]);

    }

    /**
     * Finds and displays a professor entity.
     *
     */
    public function showAction(Dancer $dancer)
    {
        $deleteForm = $this->createDeleteForm($dancer);

        return $this->render('AmiDanseBundle:Admin/Dancer:show.html.twig', array(
            'dancer' => $dancer,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing category entity.
     *
     */
    public function editAction(Request $request, Dancer $dancer)
    {
        // On vérifie que l'utilisateur dispose bien du rôle ROLE_AUTEUR
        if (!$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            // Sinon on déclenche une exception « Accès interdit »
            throw new AccessDeniedException('Accès limité aux Super-administrateurs!!!');
        }

        $deleteForm = $this->createDeleteForm($dancer);
        $editForm = $this->createForm('AmiDanseBundle\Form\AdminDancerType', $dancer);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $encoder = $this->container->get('security.password_encoder');
            $encoded = $encoder->encodePassword($dancer, $dancer->getPassword());

            $dancer->setPassword($encoded);

            $dancer->setUsername($dancer->getEmail());
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('amidanse_admin_dancer_index', array('id' => $dancer->getId()));
        }

        return $this->render('AmiDanseBundle:Admin/Dancer:edit.html.twig', array(
            'dancer' => $dancer,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a dancer entity.
     *
     */
    public function deleteAction(Request $request, Dancer $dancer)
    {
        // On vérifie que l'utilisateur dispose bien du rôle ROLE_AUTEUR
        if (!$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            // Sinon on déclenche une exception « Accès interdit »
            throw new AccessDeniedException('Accès limité aux Super-administrateurs!!!');
        }

        $form = $this->createDeleteForm($dancer);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($dancer);
            $em->flush();
        }

        return $this->redirectToRoute('amidanse_admin_dancer_index');
    }

    private function createDeleteForm(Dancer $dancer)
    {
        // On vérifie que l'utilisateur dispose bien du rôle ROLE_AUTEUR
        if (!$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            // Sinon on déclenche une exception « Accès interdit »
            throw new AccessDeniedException('Accès limité aux Super-administrateurs!!!');
        }

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('amidanse_admin_dancer_delete', array('id' => $dancer->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }
}

