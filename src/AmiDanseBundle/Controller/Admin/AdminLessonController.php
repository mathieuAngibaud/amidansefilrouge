<?php

namespace AmiDanseBundle\Controller\Admin;

use AmiDanseBundle\Entity\Lesson;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AmiDanseBundle\Form\LessonType;

class AdminLessonController extends Controller
{
    /**
     * Lists all professor entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $lessons = $em->getRepository('AmiDanseBundle:Lesson')->findAll();

        return $this->render('AmiDanseBundle:Admin/Lesson:index.html.twig', array(
            'lessons' => $lessons,
        ));
    }

    public function newAction(Request $request)
    {
        $lesson = new Lesson();

        $form = $this->createForm('AmiDanseBundle\Form\LessonType', $lesson);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($lesson);
            $em->flush();

            return $this->redirectToRoute('amidanse_admin_lesson_index');
        }

        return $this->render('AmiDanseBundle:Admin/Lesson:new.html.twig', [
            'form' => $form->createView()
        ]);

    }

    /**
     * Finds and displays a professor entity.
     *
     */
    public function showAction(Lesson $lesson)
    {
        $deleteForm = $this->createDeleteForm($lesson);

        return $this->render('AmiDanseBundle:Admin/Lesson:show.html.twig', array(
            'lesson' => $lesson,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing lesson entity.
     *
     */
    public function editAction(Request $request, Lesson $lesson)
    {
        $deleteForm = $this->createDeleteForm($lesson);
        $editForm = $this->createForm('AmiDanseBundle\Form\LessonType', $lesson);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('amidanse_admin_lesson_index', array('id' => $lesson->getId()));
        }

        return $this->render('AmiDanseBundle:Admin/Lesson:edit.html.twig', array(
            'lesson' => $lesson,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }


    /**
     * Deletes a lesson entity.
     *
     */
    public function deleteAction(Request $request, Lesson $lesson)
    {
        $form = $this->createDeleteForm($lesson);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($lesson);
            $em->flush();
        }

        return $this->redirectToRoute('amidanse_admin_lesson_index');
    }

    private function createDeleteForm(Lesson $lesson)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('amidanse_admin_lesson_delete', array('id' => $lesson->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }
}

