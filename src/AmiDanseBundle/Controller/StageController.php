<?php

namespace AmiDanseBundle\Controller;

use AmiDanseBundle\Entity\Event;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class StageController extends Controller
{
    public function indexAction()
    {
        return $this->render('AmiDanseBundle:Stage:index.html.twig');
    }
}
