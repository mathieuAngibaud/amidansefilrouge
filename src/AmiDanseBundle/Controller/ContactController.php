<?php

namespace AmiDanseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AmiDanseBundle\Entity\Dancer;

class ContactController extends Controller
{   
  
    public function indexAction(Request $request)
    {

        $form = $this->createForm('AmiDanseBundle\Form\ContactType');
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $mailContent = $this->renderView(
                'AmiDanseBundle:Contact:contactmail.html.twig',
                array('firstName' => $form['firstName']->getData(),
                    'lastName' => $form['lastName']->getData(),
                    'email' => $form['email']->getData(),
                    'message' => $form['message']->getData(),

                ));

            $message = \Swift_Message::newInstance()
                ->setSubject('Vous avez reçu une question')
                ->setFrom($form['email']->getData())
                ->setTo( 'contact.amidanse@gmail.com')
                ->setBody($mailContent,
                        'text/html'
                    );

            $this->get('mailer')->send($message);

            return $this->render('AmiDanseBundle:Home:index.html.twig');
        }


        return $this->render('AmiDanseBundle:Contact:index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}

