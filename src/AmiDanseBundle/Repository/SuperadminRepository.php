<?php

namespace AmiDanseBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * SuperadminRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class SuperadminRepository extends EntityRepository
{
}
